<!DOCTYPE html>

<html>
<head>
  <title>HTML5 Game Template</title>
  <link rel="icon" type="image/png" href="/static/img/icon/favicon.png">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link type="text/css" rel="stylesheet" href="/static/css/indexstyle.css" />

</head>

<body>
  <header>
    <img src="/static/img/header/header.png" style="padding: 0px 100px; margin-top:-90px"/>
    <iframe src="/static/dat/game.html" style="width:1025px; height:600px;border:none;" allowfullscreen></iframe>
  </header>

  <footer>
    <div class="author">
      <p> Template by DSGuy, 2018 </p>
    </div>
  </footer>

  <div class="backdrop"></div>

  <script src="/static/js/reload.min.js"></script>
</body>
</html>
