package controllers

import (
	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	c.Data["Website"] = "html5gametemplate.me"
	c.Data["Email"] = "oladejisanyaolu@gmail.com"
	c.TplName = "index.tpl"
}
